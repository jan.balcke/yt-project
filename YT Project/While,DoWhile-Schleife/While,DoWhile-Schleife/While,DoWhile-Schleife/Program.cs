﻿using System;

namespace While_DoWhile_Schleife
{
    class Program
    {
        static void Main(string[] args)
        {
            Aufgaben();
            Youtube();
        }

        static void Youtube()
        {
            int zahl = 4;

            //While-Schleife

            if(zahl<=6)
            {
                switch (zahl)
                {
                    case 1:
                        Console.WriteLine("Januar");
                    break;

                    case 2:
                        Console.WriteLine("Februar");
                    break;

                    case 3:
                        Console.WriteLine("März");
                    break;

                    case 4:
                        Console.WriteLine("April");
                    break;

                    case 5:
                        Console.WriteLine("Mai");
                    break;

                    case 6:
                        Console.WriteLine("Juni");
                    break;
                }                
            }
            else
            {
                while (zahl <= 12)
                {
                    Console.WriteLine(zahl);
                    zahl += 1;
                }

                Console.WriteLine("Zweite Hälfte des Jahres");
            }
            Console.ReadLine();


            //Do-While-Schleifen

            int zahl2 = 6;

            do
            {
                Console.WriteLine(zahl2);
                zahl2 +=1;
            }
            while (zahl2 <= 5);

            Console.ReadKey();


        }
        static void Aufgaben()
        {
            int n = Convert.ToInt32(Console.ReadLine());
            int ii = 1;
            int sum = 0;
                                   
                while (ii <= n)
                {
                    sum = sum + ii;
                    ii += 1;
                }
                Console.WriteLine(sum);
            Console.ReadKey();

        }
    }
}
